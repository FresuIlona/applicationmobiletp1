package com.example.applicationmobile_tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        Bundle intent = getIntent().getExtras();
        if(intent != null){
            String animalType = intent.getString(Intent.EXTRA_TEXT);

           // if(animalType != null){
                Animal animal = AnimalList.getAnimal(animalType);
             //   if(animal != null){
                    TextView nomAnimal = findViewById(R.id.nomAnimal);
                    nomAnimal.setText(animalType);

                    TextView esperanceVie = (TextView) findViewById(R.id.esperanceVie);
                    esperanceVie.setText(animal.getHightestLifespan());

                    TextView periodeGestation = (TextView) findViewById(R.id.periodeGestation);
                    periodeGestation.setText(animal.getGestationPeriod());

                    TextView poidsNaissance = (TextView) findViewById(R.id.poidsNaissance);
                    poidsNaissance.setText(animal.getStrBirthWeight());

                    TextView poidsAdulte = (TextView) findViewById(R.id.poidsAdulte);
                    poidsAdulte.setText(animal.getStrAdultWeight());

                    EditText statut = (EditText) findViewById(R.id.statut);
                    statut.setText(animal.getConservationStatus());
           //     }
          //  }
        }



    }
}
