package com.example.applicationmobile_tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ListView lstView;

        final ListView listview = (ListView) findViewById(R.id.lstView);
        String[] values = AnimalList.getNameArray();

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                // Do something in response to the click
                    Intent AnimalActivity = new Intent(MainActivity.this,AnimalActivity.class);
                    startActivity(AnimalActivity);
                }
        });
    }
}
